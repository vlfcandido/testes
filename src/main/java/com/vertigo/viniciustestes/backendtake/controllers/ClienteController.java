package com.vertigo.viniciustestes.backendtake.controllers;

import com.vertigo.viniciustestes.backendtake.entity.Cliente;
import com.vertigo.viniciustestes.backendtake.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class ClienteController {
    @Autowired
    private ClienteRepository clienteRepository;

    @RequestMapping(value = "/clientes", method = RequestMethod.GET)
    public List<Cliente> getAll() {
        List<Cliente> listaClientes;
        listaClientes = clienteRepository.findAll();
        for (Cliente c : listaClientes) {
            System.out.println("Cliente:");
            System.out.println(c.getIdcliente());
            System.out.println(c.getNome());
            System.out.println(c.getCpfcnpj());
        }
        return clienteRepository.findAll();
    }

    @RequestMapping(value = "/cliente/{id}", method = RequestMethod.GET)
    public ResponseEntity<Cliente> getById(@PathVariable(value = "id") long id)
    {
        Optional<Cliente> pessoa = clienteRepository.findById(id);
        if(pessoa.isPresent()) {
            System.out.println("Encontrou");
            return new ResponseEntity<Cliente>(pessoa.get(), HttpStatus.OK);
        } else {
            System.out.println("Não encontrou");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/cliente", method =  RequestMethod.POST)
    public Cliente insert(@RequestBody Cliente cliente)
    {
        return clienteRepository.save(cliente);
    }

    @RequestMapping(value = "/cliente/{id}", method =  RequestMethod.PUT)
    public ResponseEntity<Cliente> update (@PathVariable(value = "id") long id, @RequestBody Cliente newCliente)
    {
        Optional<Cliente> oldCliente = clienteRepository.findById(id);
        if(oldCliente.isPresent()){
            Cliente cliente = oldCliente.get();
            cliente.setNome(newCliente.getNome());
            clienteRepository.save(cliente);
            return new ResponseEntity<Cliente>(cliente, HttpStatus.OK);
        }
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/cliente/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> delete (@PathVariable(value = "id") long id)
    {
        Optional<Cliente> cliente = clienteRepository.findById(id);
        if(cliente.isPresent()){
            clienteRepository.delete(cliente.get());
            return new ResponseEntity<>(HttpStatus.OK);
        }
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}