package com.vertigo.viniciustestes.backendtake.controllers;

import com.vertigo.viniciustestes.backendtake.BackendTakeApplication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RestartController {

    @PostMapping("/restart")
    public void restart() {
        BackendTakeApplication.restart();
    }
}