package com.vertigo.viniciustestes.backendtake.repository;

import com.vertigo.viniciustestes.backendtake.entity.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Long> { }